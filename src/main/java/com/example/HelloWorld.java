package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloWorld {

	@GetMapping("/message")
	public String getMessage() {
		return "Spring Application is working properly! - new feature";
	}

	public static void main(String[] args) {
		SpringApplication.run(HelloWorld.class, args);
	}

	public String sayHello() {
		return "Hello!";
	}

}
